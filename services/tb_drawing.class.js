/**
 * TbDrawing Class
 *
 * Class
 *
 * @author David LOIRET < david.loiret@spallian.com >
 * @license Spallian
 * @version 0.0.2
 * @since 2015-01-01 | last update 2015-02-01
 * @memberOf tbDrawing
 * inspired by sketch.js
 */
//color picker as webstorm
//edgedetect -> colorier dans ne image
//enlever tout ce qui depasse de limage (calque photoshop)
//creer c propre outil (avec picture ...)
//use d3js to create tool or graph or other things
//pour offset parcourir tt les parents
//http://ionicons.com/
//garder historique des elements dessinés (new Point, new Vector, new Polygo stocké dans vector ou layer class comme corto api)
//https://github.com/szimek/signature_pad/blob/master/src/signature_pad.js
//http://www.html5rocks.com/en/tutorials/canvas/performance/
//webgl
//use injector to inject tool on demand? or stay window[tool] or object key -> value and assess tool by value
//dynamically set z index
//drag image to add picture to layer
//create tool (user) with image and effect
//make canvas inside div pour gérer plusieurs canvas
//save current picture inisde localstorage
//http://www.rgraph.net/blog/2013/january/html5-canvas-dashed-lines.html
//https://github.com/kig/canvasfilters
//http://lodev.org/cgtutor/filtering.html
//http://en.wikipedia.org/wiki/Kernel_%28image_processing%29
//http://beej.us/blog/data/convolution-image-processing/
//http://docs.gimp.org/en/plug-in-convmatrix.html
//http://web.pdx.edu/~jduh/courses/Archive/geog481w07/Students/Ludwig_ImageConvolution.pdf
//http://lodev.org/cgtutor/filtering.html
//http://wcolorpicker.websanova.com/
//https://github.com/mihaisucan/PaintWeb/blob/master/src/tools/polygon.js
//listen espace
//use svg http://www.w3schools.com/svg/svg_inhtml.asp
//http://www.sitepoint.com/10-color-pickers-plugin/
//use underscore instead of angular for extend.. or lodash
//ne pas faire de service mais direct class js?
//http://javascript.info/tutorial/native-prototypes
//http://weavesilk.com/
//limit de tps
//compare drawing
//user can add picture
//categorie
//class layer
//http://www.dbp-consulting.com/tutorials/canvas/CanvasArrow.html
//https://github.com/pashanitw/paint/blob/master/app/scripts/canvas/directive.js
//@TODO add opacity
//move object
//select (rectangle) and filter on select
//select draw path and filter on path
//shape
//texture
//tool
//filters
//fillcolor
//arrow stroke oupas
// opacité
//stroke color
//faire un objet tools (a envoyer a la fonction draw?)
// arrow = {stroked..;}
//fixed head length proportional length
//infinité de layer -> use canvaslayer?
//user can choose the layer
//adaptation au touchstart...
//http://www.w3schools.com/html/html5_canvas.asp
//effect
//http://docs.gimp.org/en/plug-in-convmatrix.html
// filtre passe bas filtre passe haut
//http://stephanieluu.com/image-convolution/
//faire adaptation jquery
//apply filter on drawing
//apply filter on selection
//https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes
//https://dev.opera.com/articles/html5-canvas-painting/
//keyboard short cut
//new tools (each tool must have porpeties)
//https://code.google.com/p/paintweb/
//ADD ANIMATION
//http://perfectionkills.com/exploring-canvas-drawing-techniques/
//http://www.rgraph.net/blog/2013/april/an-example-of-the-html5-canvas-path-functions.html
//http://curran.github.io/HTML5Examples/
//add support for touch event
//make a class for each tool
//make class for filters
//possible multi layer
//stroke or not
//la c'est moins lourd
//PUT DIRECTIVE CODE INTO SERVICE
//https://thiscouldbebetter.wordpress.com/2013/08/14/filtering-images-with-convolution-masks-in-javascript/
//http://blog.ivank.net/fastest-gaussian-blur.html
//gestion local storage (ds controller)
//gérer effet
//faire class curve -> intancie pas si pas besoin
//event par tool (chaque tool a une classe -> curve ensemble method si trop diff une autre class)
//qd kill outil killer class?
//polygon
//selection
//ajout de layer a la volé -> gerer zindex ajouter image copier bout image collé
//appliquer outil sur selection et masque ...
//remplacer this output par selected canvas -> context de lapplication des outils
//class common tool
//class common filtre
//short cut
//charger uniquement ce dont on a besoin
//use buffer and active
//fonction to addLayer (or class layer as cwvcanvas, cwvector ...
//add text tool
//resize image
//http://stackoverflow.com/questions/2303690/resizing-an-image-in-an-html5-canvas
//use web workers
//http://www.w3schools.com/tags/canvas_drawimage.asp
//http://stackoverflow.com/questions/18922880/html5-canvas-resize-downscale-image-high-quality
//https://gist.github.com/fisch0920/37bac5e741eaec60e983
//https://github.com/calvintwr/Hermite-resize
//https://html.spec.whatwg.org/multipage/scripting.html#attr-canvas-width
//keep positio (new Point) ...
//http://www.ecma-international.org/ecma-262/5.1/
//http://hangar.runway7.net/javascript/difference-call-apply
//http://tympanus.net/codrops/2014/10/30/resizing-cropping-images-canvas/
// ============Compare Image ================
//http://www.imagemagick.org/Usage/compose/
//http://rhnh.net/2011/08/07/ocr-with-clojure-and-imagemagick
//http://vbridge.co.uk/2012/11/05/how-we-tuned-tesseract-to-perform-as-well-as-a-commercial-ocr-package/
//http://www.fmwconcepts.com/imagemagick/
//http://www.imagemagick.org/script/compare.php
//https://github.com/mash/node-imagemagick-native
//https://www.npmjs.com/package/imagemagick
//http://stackoverflow.com/questions/5132749/imagemagick-diff-an-image
//pendu live
//live drawing
//modification graph discngine stage en live
//http://stackoverflow.com/questions/10794409/meteor-automatically-updating-canvas-with-subscribed-data
//https://medium.com/@zfxuan/the-wonderful-duo-using-meteor-and-angularjs-together-4d603a4651bf
/*
 * ================================================================================================
 * ============================================================add
 * SpMap DEFINITION
 *
 * ================================================================================================
 * ================================================================================================
 */
//canvas
//layer
//tools
(function () {

  'use strict';

  angular
    .module('tbDrawing')
    .factory('TbDrawing', TbDrawing);

  /**
   * @ngInject
   */
  function TbDrawing ($window, $timeout, tbLocalStorage, $q) {

    var TbDrawing = function (options, canvas) {
      // Default options
      var defaultOptions = {
        toolLinks: true,
        tool: 'Curve',
        strokeStyle: '#000000',
        fillStyle: 'blue',
        lineWidth: 10,
        size: 5,
        // Callback_init
        callbackInit: function () {/* Do Nothing */
        }
      };

      this.options = angular.extend({}, defaultOptions, options);

      // Merge options
      this.canvas = canvas;
      this.layers = {
        output: this.canvas.output.getContext('2d'),
        drawing: this.canvas.drawing.getContext('2d'),
        background: this.canvas.background.getContext('2d')
      };
      this.layers.drawing.globalCompositeOperation = 'source-over';
      this.layers.output.globalCompositeOperation = 'source-over';
      var self = this;
      this.toolOptions = {
        shadowBlur: 10,//mettre ds options default
        shadowColor: 'rgb(0, 0, 0)',
        stroked: false,
        strokeStyle: self.options.strokeStyle,
        fillStyle: self.options.fillStyle,
        selectedLayer: 'output',
        lineWidth: self.options.lineWidth
      };

      //@TODO remove background image from output
      this.log = new TbDrawingLog(this.canvas, this.background);
      this.tool = null;
      this.setTool(this.options.tool);
      this.storage = false;

      this.init();
      //addlayer or add canvas

      this.background = this.options.background;
      this.backgroundImage = new Image();
      this.backgroundImage.src = this.background;
      var _self = this;
      this.eventActions = {
        touchstart: _self.startPainting,
        mousedown: _self.startPainting,
        mouseleave: _self.stopPainting,
        touchcancel: _self.stopPainting,
        mouseup: _self.stopPainting,
        touchend: _self.stopPainting
      };

      var drawOptions = tbLocalStorage.get('toolOptions');
      if (drawOptions) {
        this.toolOptions = angular.extend({}, this.toolOptions, drawOptions);
      }


      if (!this.backgroundImage.src) return false;
      this.backgroundImage.onload = function () {
        self.layers.background.drawImage(self.backgroundImage, 0, 0);
      };
    };

    //Héritage Curve  hérie de common
    TbDrawing.prototype = Object.create(Common.prototype);//passer common in tools?
    TbDrawing.prototype.constructor = TbDrawing;
    /*
     * ================================================================================================
     * ================================================================================================
     *
     * GETTERS / SETTERS
     *
     * ================================================================================================
     * ================================================================================================
     */

    /**
     * init
     */
    TbDrawing.prototype.init = function () {
      tbLocalStorage.set('toolOptions', this.toolOptions);//store choosen tool
      if (!this.storage) return false;
      var base64 = tbLocalStorage.get('drawPicture');
      var image = new Image();
      image.src = base64;
      if (!image.src) return false;
      var ctx = this.layers.output;
      image.onload = function () {
        ctx.drawImage(image, 0, 0);
      };
    };

    TbDrawing.prototype.addImage = function (base64, layer, maxSize) {
      var _self = this;
      var image = new Image();
      image.src = base64;
      if (!image.src) return false;
      image.onload = function () {
        var size = _self.getRatioSize(maxSize, image);
        _self.layers[layer].drawImage(image, 0, 0, size.width, size.height);
      };
    };

    TbDrawing.prototype.getRatioSize = function (maxSize, image) {
      var maxWidth = maxSize.width;
      var maxHeight = maxSize.height;
      var imageWidth = image.width;
      var imageHeight = image.height;

      if (imageWidth > imageHeight) {
        if (imageWidth > maxWidth) {
          imageHeight *= maxWidth / imageWidth;
          imageWidth = maxWidth;
        }
      }
      else {
        if (imageHeight > maxHeight) {
          imageWidth *= maxHeight / imageHeight;
          imageHeight = maxHeight;
        }
      }

      var size = {
        width: imageWidth,
        height: imageHeight
      }

      return size;
    }

    /**
     * getElement
     */
    TbDrawing.prototype.getCanvas = function () {
      return this.options.canvas;
    };

    /**
     * setStrokeStyle
     */
    TbDrawing.prototype.setStrokeStyle = function (strokeStyle) {
      this.toolOptions.strokeStyle = strokeStyle;
    };

    /**
     * setFillSTyle
     */

    TbDrawing.prototype.setFillStyle = function (fillStyle) {
      this.toolOptions.fillStyle = fillStyle;
    };

    /**
     * setlineWidth
     */

    TbDrawing.prototype.setLineWidth = function (lineWidth) {
      this.toolOptions.lineWidth = lineWidth;
    };

    /*
     * ================================================================================================
     * ================================================================================================
     *
     * FONCTIONS
     *
     * ================================================================================================
     * ================================================================================================
     */
    /**
     * download
     */

    TbDrawing.prototype.download = function (options, open) {
      var deferred = $q.defer();
      var self = this;
      var mime;
      options.format = options.format || 'png';
      if (options.format === 'jpg') {
        options.format = 'jpeg';
      }
      mime = 'image/' + options.format;

      var image = new Image();
      image.src = self.canvas.background.toDataURL();
      //store tmp output
      var drawingImage = new Image();
      drawingImage.src = self.canvas.output.toDataURL();
      if (!drawingImage.src) return false;
      image.onload = function () {
        //get background image
        self.layers.output.globalCompositeOperation = 'destination-over';
        self.layers.output.drawImage(image, 0, 0, size.width, size.height);
        var data = self.canvas.output.toDataURL(mime);
        if (open) {
          var url = data.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
          $window.open(url);
          //document.location.href = data;
        }
        self.clear(self.canvas.output);
        self.layers.output.globalCompositeOperation = 'source-over';
        var size = self.getRatioSize(options.size, image);

        self.layers.output.drawImage(drawingImage, 0, 0);
        deferred.resolve(data);
      };

      return deferred.promise;

    };


    /**
     * rm clear localstorage and logs
     */
    TbDrawing.prototype.rm = function () {
      this.clear(this.canvas.output);
      tbLocalStorage.set('drawPicture', null);//use remove bykey
      this.log.rmAll();
    };

    /**
     * rm clear localstorage and logs
     */
    TbDrawing.prototype.rmAll = function () {
      this.clear(this.canvas.output);
      this.clear(this.canvas.drawing);
      this.clear(this.canvas.background);
      tbLocalStorage.set('drawPicture', null);//use remove bykey
      this.log.rmAll();
    };

    /**
     * setTool
     */
    TbDrawing.prototype.setTool = function (tool) {
      var options = {
        id: tool,
        canvas: this.canvas,
        layers: this.layers,
        background: this.background,
        toolOptions: this.toolOptions
      };
      if (this.tool !== 'object' || this.tool.id !== tool) {//avoid creating new object if already
        //tool += 'Tool';
        this.tool = new window[tool](options);//injector $get['tool']
      }
    };

    /**
     * setSelectedLayer
     */
    TbDrawing.prototype.setSelectedLayer = function (layer) {
      this.selectedLayer = layer;
    };

    /**
     * Start Painting
     */
    TbDrawing.prototype.startPainting = function () {
      this.painting = true;
    };

    TbDrawing.prototype.clearTimeout = function () {
      console.log('clear Timeout')
      var _self = this;
      if (_self.timeout) {
        $timeout.cancel(_self.timeout)
      }
    };

    TbDrawing.prototype.stopPainting = function () {
      var self = this;
      if (this.painting) {
        var drawingImage = new Image();
        drawingImage.src = this.canvas.drawing.toDataURL();
        if (!drawingImage.src) return false;
        drawingImage.onload = function () {//faire cette methodo uniquement au clear du canvas drawing to improve performance
          self.layers.output.drawImage(drawingImage, 0, 0);
          self.layers.drawing.clearRect(0, 0, self.canvas.drawing.width, self.canvas.drawing.height);
          if (self.storage) {
            //self.clear(self.canvas.drawing);//avoid flipping we clear une fois que c'est dessiné sur le output
            self.log.logDrawing();//workers
            self.timeout = $timeout(function () {
              // sauvegarder on destroy du scope ou page leave mais pas a chaque fois ou killer le timeout
              var base64 = self.canvas.output.toDataURL();
              tbLocalStorage.set('drawPicture', base64);
            }, 200);
          }
        };
      }
      this.painting = false;
    };

    TbDrawing.prototype.onEvent = function (e) {//in common
      var _self = this;
      e.preventDefault();
      _self.tool.onEvent(e);//faire pareil pour les outil
      if (_self.eventActions[event.type]) {//put in workers?
        return _self.eventActions[event.type].apply(_self);//faire un tool .startPainting .. pas de onEvent
      }
      return false;
    };

    TbDrawing.prototype.undo = function () {//faire de la hierarachie voir clement api
      this.log.undo();
    };

    TbDrawing.prototype.redo = function () {
      this.log.redo();
    };


    TbDrawing.prototype.drawTimeBasedSprayRound = function () {
      //var density = 50;

    };

    TbDrawing.prototype.drawTriangle = function () {
    };

    TbDrawing.prototype.drawPolygon = function () {
    };


    TbDrawing.prototype.getPixels = function (canvas) {//pass img
      var ctx = canvas.getContext('2d');
      return ctx.getImageData(0, 0, canvas.width, canvas.height);
    };

    //put in separate class
    //filters http://www.html5rocks.com/en/tutorials/canvas/imagefilters/?redirect_from_locale=fr
    TbDrawing.prototype.filterImage = function (filter, options, options2) {
      var args = [this.getPixels(this.canvas[this.selectedLayer])];
      for (var i = 2; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
      var pixels = this[filter](args[0], options, options2);
      this.layers[this.selectedLayer].putImageData(pixels, 0, 0);
      this.log.logDrawing(this.selectedLayer);
      return pixels;
    };

    TbDrawing.prototype.grayscale = function (pixels) {
      var d = pixels.data;
      for (var i = 0; i < d.length; i += 4) {
        var r = d[i];
        var g = d[i + 1];
        var b = d[i + 2];
        // CIE luminance for the RGB
        var v = 0.2126 * r + 0.7152 * g + 0.0722 * b;
        d[i] = d[i + 1] = d[i + 2] = v;
      }
      return pixels;
    };

    TbDrawing.prototype.brightness = function (pixels, adjustment) {
      var d = pixels.data;
      for (var i = 0; i < d.length; i += 4) {
        d[i] += adjustment;
        d[i + 1] += adjustment;
        d[i + 2] += adjustment;
      }
      return pixels;
    };


    TbDrawing.prototype.threshold = function (pixels, threshold) {
      var d = pixels.data;
      for (var i = 0; i < d.length; i += 4) {
        var r = d[i];
        var g = d[i + 1];
        var b = d[i + 2];
        var v = (0.2126 * r + 0.7152 * g + 0.0722 * b >= threshold) ? 255 : 0;
        d[i] = d[i + 1] = d[i + 2] = v;
      }
      return pixels;
    };

    TbDrawing.prototype.createImageData = function (w, h) {
      var tmpCanvas = document.createElement('canvas');
      var tmpCtx = tmpCanvas.getContext('2d');
      return tmpCtx.createImageData(w, h);
    };


    /*            var side = Math.round(Math.sqrt(weights.length));
     var halfSide = Math.floor(side/2);
     var src = pixels.data;
     var sw = pixels.width;
     var sh = pixels.height;
     // pad output by the convolution matrix
     var w = sw;
     var h = sh;
     var output = this.createImageData(w, h);
     var dst = output.data;
     // go through the destination image pixels
     var alphaFac = opaque ? 1 : 0;
     for (var y = 0; y < h; y++) {
     for (var x = 0; x < w; x++) {
     var sy = y;
     var sx = x;
     var dstOff = (y * w + x) * 4;
     // calculate the weighed sum of the source image pixels that
     // fall under the convolution matrix
     var r = 0, g = 0, b = 0, a = 0;
     for (var cy = 0; cy < side; cy++) {
     for (var cx = 0; cx < side; cx++) {
     var scy = sy + cy - halfSide;
     var scx = sx + cx - halfSide;
     if (scy >= 0 && scy < sh && scx >= 0 && scx < sw) {
     var srcOff = (scy * sw + scx) * 4;
     var wt = weights[cy * side + cx];
     r += src[srcOff] * wt;
     g += src[srcOff + 1] * wt;
     b += src[srcOff + 2] * wt;
     a += src[srcOff + 3] * wt;
     }
     }
     }
     dst[dstOff] = r;
     dst[dstOff + 1] = g;
     dst[dstOff + 2] = b;
     dst[dstOff + 3] = a + alphaFac * (255 - a);
     }
     }
     return output;*/
    TbDrawing.prototype.convolute = function (pixels, weights, opaque) {
      var side = Math.round(Math.sqrt(weights.length));
      var halfSide = Math.floor(side / 2);

      var src = pixels.data;
      var sw = pixels.width;
      var sh = pixels.height;

      var w = sw;
      var h = sh;
      var output = this.createImageData(w, h);
      var dst = output.data;

      var alphaFac = opaque ? 1 : 0;

      for (var y = 0; y < h; y++) {
        for (var x = 0; x < w; x++) {
          var sy = y;
          var sx = x;
          var dstOff = (y * w + x) * 4;
          var r = 0, g = 0, b = 0, a = 0;
          // calculate the weighed sum of the source image pixels that
          // fall under the convolution matrix
          for (var cy = 0; cy < side; cy++) {
            for (var cx = 0; cx < side; cx++) {
              var scy = Math.min(sh - 1, Math.max(0, sy + cy - halfSide));
              var scx = Math.min(sw - 1, Math.max(0, sx + cx - halfSide));
              var srcOff = (scy * sw + scx) * 4;
              var wt = weights[cy * side + cx];
              r += src[srcOff] * wt;
              g += src[srcOff + 1] * wt;
              b += src[srcOff + 2] * wt;
              a += src[srcOff + 3] * wt;
            }
          }
          dst[dstOff] = r;
          dst[dstOff + 1] = g;
          dst[dstOff + 2] = b;
          dst[dstOff + 3] = a + alphaFac * (255 - a);
        }
      }
      return output;
    };

    TbDrawing.prototype.testsobel = function () {
      var px = this.grayscale(this.getPixels(this.canvas[this.selectedLayer]));
      var vertical = this.convolute(px,
        [-1, -2, -1,
          0, 0, 0,
          1, 2, 1]);
      var horizontal = this.convolute(px,
        [-1, 0, 1,
          -2, 0, 2,
          -1, 0, 1]);

      this.clear(this.canvas.output);
      var id = this.layers.output.createImageData(vertical.width, vertical.height);

      for (var i = 0; i < id.data.length; i += 4) {
        var v = Math.abs(vertical.data[i]);
        id.data[i] = v;
        var h = Math.abs(horizontal.data[i]);
        id.data[i + 1] = h;
        id.data[i + 2] = (v + h) / 4;
        id.data[i + 3] = 255;
      }

      this.layers[this.selectedLayer].putImageData(id, 0, 0);
      return id;
    };

    TbDrawing.prototype.resize = function (width, height) {
      var self = this;
      angular.forEach(self.canvas, function (value, key) {
        this.canvas[key].width = width;
        this.canvas[key].height = height;
      });
    };

    return TbDrawing;

  }

}());
