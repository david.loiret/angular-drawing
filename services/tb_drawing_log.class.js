//faire objet avec layer associé à lhistorique
function TbDrawingLog (canvas, background) {
	this.isFirefox = typeof InstallTrigger !== 'undefined'; //avoid lantency (with onload do the same in the tb_drawing class for drawing on output?)
	this.canvas = canvas;//liste des canvas
	this.index = 0;
	this.logs = [];
	this.background = background;
}
//sauver tout les layer pour chaque etapes et tt remettre

TbDrawingLog.prototype.getLogs = function () {
	return this.logs;
};

TbDrawingLog.prototype.sliceAndPush = function(imageObject) {
    var array;
    //keep only a specific number of elements
/*		    if (this.logs.length > 4) {
    	var indexToRemove = 0;
		var numberToRemove = 4;

		this.logs.splice(indexToRemove, numberToRemove);
		this.index -= 3;
    }*/
    if (this.index === this.logs.length - 1) {
        this.logs.push(imageObject);
        array = this.logs;
    } else {
        var tempArray = this.logs.slice(0, this.index + 1);
        tempArray.push(imageObject);
        array = tempArray;
    }
    if (array.length > 1) {
        this.index++;
    }
    return array;
};

TbDrawingLog.prototype.logDrawing = function(selectedLayer) { 
    var object = {};
    var self = this;
    var image;
    if (this.isFirefox) {
    	angular.forEach(self.canvas, function (value, key) {
    		if (key !== 'drawing') {
	    		image = new Image();
	        	image.src = self.canvas[key].toDataURL();
	        	object[key] = image;
    		}
    	});
        this.logs = this.sliceAndPush(object);
    } else {
    	angular.forEach(self.canvas, function (value, key) {
    		if (key !== 'drawing') {
        		image = self.canvas[key].toDataURL();
        		object[key] = image;
    		}
    	});
        this.logs = this.sliceAndPush(object);
    }
};

TbDrawingLog.prototype.undo = function() {
    if (this.index > 0) {
        this.index--;
        this.showLogAtIndex(this.index);
    } else {
    	//on clean les layers
    	var ctx;
    	var self = this;
    	var image = new Image();
        if (!image.src) return false;
    	image.src = this.background;//revoir la méthodo -> stocker etat initial? pluto que de le recréer
        image.onload = function() {
        	ctx = self.canvas.background.getContext('2d');
    		ctx.clearRect(0, 0, self.canvas.background.width, self.canvas.background.height);
            ctx.drawImage(image, 0, 0);
        };
        ctx = self.canvas.output.getContext('2d');
        ctx.clearRect(0, 0, self.canvas.output.width, self.canvas.output.height);
    }
};

TbDrawingLog.prototype.redo = function() {
    if (this.index < this.logs.length - 1) {
        this.index++;
        this.showLogAtIndex(this.index);
    }
};

TbDrawingLog.prototype.rmAll = function () {
    this.logs = [];
    this.index = 0;
};

TbDrawingLog.prototype.showLogAtIndex = function(index) {
	var object = this.logs[index];
	var self = this;

    if (!Object.keys(object).length) return false;
	angular.forEach(self.canvas, function (value, key) {
		if (key !== 'drawing') {
			var canvas = self.canvas[key];
		    var ctx = canvas.getContext('2d');
		    if (self.isFirefox) {
		    	ctx.clearRect(0, 0, canvas.width, canvas.height);
		        var image = object[key];
		        ctx.drawImage(image, 0, 0);
		    } else {
		        var image = new Image();
		        image.src = object[key];
		        image.onload = function () {
		   			ctx.clearRect(0, 0, canvas.width, canvas.height);
		        	ctx.drawImage(image, 0, 0);
		        };
		    }
    		
		}
	});
};
