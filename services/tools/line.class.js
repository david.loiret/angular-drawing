/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* Line Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe Line, 
* Permet la création et la manipulation de line
* @class Line
* @param {Object} options  Données de la ligne
* @constructor
*/
function Line (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'line',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage line  hérie de common 
Line.prototype = Object.create(Common.prototype);
Line.prototype.constructor = Line;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
Line.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
Line.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
Line.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
Line.prototype.stopPainting = function () {
    this.painting = false;
};

Line.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};

Line.prototype.draw = function () {
	var ctx = this.layers.drawing;
	this.clear(this.canvas.drawing);
	ctx.save();
	ctx.beginPath();
	ctx.lineJoin = 'miter';
	ctx.lineCap = 'miter';
	ctx.moveTo(this.firstPoint.x, this.firstPoint.y);
	ctx.lineTo(this.currentPoint.x, this.currentPoint.y);
	ctx.lineWidth = this.toolOptions.lineWidth;
	ctx.strokeStyle = this.toolOptions.strokeStyle;
	ctx.stroke();
	ctx.closePath();
	ctx.restore();
};



