/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* Common Class
*
* ________________________________________________________________________________________________
*/
/**
* Déclaration de la classe Common,
* Permet la création et la manipulation de line
* @class Common
* @param {Object} options  Données de la ligne
* @constructor
*/
'use strict';
function Common (options) {
	// Default options
	var defaultOptions = {
		// Callback_init
		callbackInit: function() {/* Do Nothing */}
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key];
	}

	// Initialisation
	if (this.init() === true) {
		this.callbackInit();
	}

}

/**
* Prototype definition
*/

/*
* ================================================================================================
* ================================================================================================
*
* INITIALISATION
*
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
Common.prototype.init = function() {

	return true;
};

/*
* ================================================================================================
* ================================================================================================
*
* GETTERS / SETTERS
*
* ================================================================================================
* ================================================================================================
*/


/*
* ================================================================================================
* ================================================================================================
*
* FONCTIONS
*
* ================================================================================================
* ================================================================================================
*/

Common.prototype.setPoint = function (e) {//mettre dans un common
    var trueOffset = this.findBoundingBox(this.canvas.output);

    var offsetLeft = trueOffset.offsetLeft;
    var offsetTop = trueOffset.offsetTop;

    var x = e.clientX;
    var y = e.clientY;
    if (e.originalEvent && e.originalEvent.changedTouches) {
        x = e.originalEvent.changedTouches[0].clientX;
        y = e.originalEvent.changedTouches[0].clientY;
    }
    //gestion multi touch avec gestion stack de point
    var point = {
        x: x - offsetLeft,
        y: y - offsetTop
    };
    return point;
};

//http://www.quirksmode.org/js/findpos.html
Common.prototype.findPos = function (obj) {
    var curleft = 0;
    var curtop = 0;

    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return {offsetLeft: curleft, offsetTop: curtop};
    }
};
//http://www.html5canvastutorials.com/advanced/html5-canvas-mouse-coordinates/
Common.prototype.findBoundingBox = function (canvas) {
  var rect = canvas.getBoundingClientRect();
  return {offsetLeft: rect.left, offsetTop: rect.top};
};

/**
* clear canvas
*/
Common.prototype.clear = function (canvas) {
    canvas = canvas || this.canvas.output;
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
};


//a mettre dans class mere abstraite(les common mc)

Common.prototype.distanceBetween = function (point1, point2) {
    return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
};

Common.prototype.angleBetween = function (point1, point2) {
    return Math.atan2( point2.x - point1.x, point2.y - point1.y );
};

Common.prototype.getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

Common.prototype.getRandomFloat = function (min, max) {
    return Math.random() * (max - min) + min;
};
