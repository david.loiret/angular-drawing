/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* Arrow Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe Arrow, 
* Permet la création et la manipulation de Curve
* @class Arrow
* @param {Object} options  Données de la ligne
* @constructor
*/
function Arrow (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
Arrow.prototype = Object.create(Common.prototype);
Arrow.prototype.constructor = Arrow;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
Arrow.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
Arrow.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
Arrow.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
Arrow.prototype.stopPainting = function () {
    this.painting = false;
};

Arrow.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        case 'touchstart':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//http://askeralim.blogspot.fr/2013/01/draw-arrow-in-html5-canvas.html
Arrow.prototype.draw = function () {
    var ctx = this.layers.drawing;
    this.clear(this.canvas.drawing);
    //new arrow .draw?
    function Point(x, y) {
      this.x = x;
      this.y = y;
    }
    var arrow1FromPoint = this.firstPoint;
    var arrow1ToPoint = this.currentPoint;
    //drawArrow(ctx,arrow1FromPoint,arrow1ToPoint);
    //drawArrow(ctx,arrow2FromPoint,arrow2ToPoint,!stroked);
    // Drawing Arrow Head Stroked. 
    this.strokeArrowHead = function (ctx,toPoint,arrowPoint1,arrowPoint2){
        ctx.beginPath();
        ctx.moveTo(toPoint.x,toPoint.y);
        ctx.lineTo(arrowPoint1.x,arrowPoint1.y);
        ctx.lineTo(arrowPoint2.x,arrowPoint2.y);
        ctx.lineTo(toPoint.x,toPoint.y);
        ctx.fillStyle = this.fillStyle;
        ctx.strokeStyle = this.strokeStyle;
        ctx.fill();
        ctx.stroke();
        ctx.closePath();
    };

    this.arrowHeadLines = function (ctx,toPoint,arrowPoint1,arrowPoint2){
        ctx.beginPath();
        ctx.moveTo(arrowPoint1.x, arrowPoint1.y);
        ctx.lineTo(toPoint.x, toPoint.y);
        ctx.lineTo(arrowPoint2.x, arrowPoint2.y);
        ctx.stroke();
        ctx.closePath();
    };

    this.printArrow = function (ctx, fromPoint, toPoint) {
        var dx = toPoint.x - fromPoint.x;
        var dy = toPoint.y - fromPoint.y;
        // normalize
        var length = Math.sqrt(dx * dx + dy * dy);
        var unitDx = dx / length;
        var unitDy = dy / length;
        // increase this to get a larger arrow head
        var arrowHeadSize =  Math.sqrt((this.toolOptions.lineWidth * 10) * (length / 10));//depend longueur de la fléche?

        var arrowPoint1 = new Point(
            (toPoint.x - unitDx * arrowHeadSize - unitDy * arrowHeadSize),
            (toPoint.y - unitDy * arrowHeadSize + unitDx * arrowHeadSize));
        var arrowPoint2 = new Point(
            (toPoint.x - unitDx * arrowHeadSize + unitDy * arrowHeadSize),
            (toPoint.y - unitDy * arrowHeadSize - unitDx * arrowHeadSize));
     
        ctx.fillStyle = "rgba(100, 0, 200, 0.5)";
        // Drawing Arrow Line.
        ctx.beginPath();
        ctx.moveTo(fromPoint.x,fromPoint.y);
        ctx.lineTo(toPoint.x,toPoint.y);
        ctx.closePath();
        ctx.lineWidth = this.toolOptions.lineWidth;
        ctx.stroke();
        if (this.stroked) this.strokeArrowHead(ctx, toPoint, arrowPoint1, arrowPoint2);
        else this.arrowHeadLines(ctx, toPoint, arrowPoint1, arrowPoint2);
    };
    this.printArrow(ctx, arrow1FromPoint, arrow1ToPoint);
};