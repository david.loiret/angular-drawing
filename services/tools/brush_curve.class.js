/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* BrushCurve Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe BrushCurve, 
* Permet la création et la manipulation de Curve
* @class BrushCurve
* @param {Object} options  Données de la ligne
* @constructor
*/
function BrushCurve (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'BrushCurve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
BrushCurve.prototype = Object.create(Common.prototype);
BrushCurve.prototype.constructor = BrushCurve;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
BrushCurve.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
BrushCurve.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
BrushCurve.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
BrushCurve.prototype.stopPainting = function () {
    this.painting = false;
};

BrushCurve.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        case 'touchstart':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//use shiftkey do same as rectangle

BrushCurve.prototype.draw = function () {
    var ctx = this.layers.drawing;
    ctx.save();
    var self = this;
    var x;
    var y;
    var dist = this.distanceBetween(this.previousPoint, this.currentPoint);
    var angle = this.angleBetween(this.previousPoint, this.currentPoint);
    var img = new Image();
    img.src = 'app/modules/tb_drawing/assets/img/brush2.png';

    img.onload = function () {
        for (var i = 0; i < dist; i++) {
            x = self.previousPoint.x + (Math.sin(angle) * i) - 25;
            y = self.previousPoint.y + (Math.cos(angle) * i) - 25;
            ctx.drawImage(img, x, y);
        }
        ctx.restore();
    };
};
