/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* Select Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe Select, 
* Permet la création et la manipulation de Curve
* @class Select
* @param {Object} options  Données de la ligne
* @constructor
*/
function Select (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Select',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
Select.prototype = Object.create(Common.prototype);
Select.prototype.constructor = Select;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
Select.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
Select.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
Select.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
Select.prototype.stopPainting = function () {
    this.painting = false;
};

Select.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


Select.prototype.draw = function () {
    var ctx = this.layers.drawing;
    ctx.save();
    this.clear(this.canvas.drawing);
    ctx.beginPath();
    ctx.lineJoin = 'miter';
    ctx.lineCap = 'miter';
    var x = (this.currentPoint.x - this.firstPoint.x < 0) ? this.currentPoint.x : this.firstPoint.x;
    var y = (this.currentPoint.y - this.firstPoint.y < 0) ? this.currentPoint.y : this.firstPoint.y;
    var w = Math.abs(this.currentPoint.x - this.firstPoint.x);
    var h = Math.abs(this.currentPoint.y - this.firstPoint.y);
    //check shiftKey
    if (this.shiftKey) {
        if (w > h) {
            //for returning the Select
            if (y === this.currentPoint.y) {
                y -= w - h;
            }
            h = w;
        } else {
            if (x === this.currentPoint.x) {
                x -= h - w;
            }
            w = h;
        }
    }

    ctx.moveTo(this.previousPoint.x, this.previousPoint.y);
    ctx.rect(x, y, w, h);
    ctx.setLineDash([2,2]);
    //ctx.fillStyle = 'yellow';
    ctx.lineWidth = 5;
    ctx.strokeStyle = this.toolOptions.strokeStyle;
    ctx.stroke();
    ctx.restore();
    ctx.closePath();
};
