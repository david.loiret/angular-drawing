/**
 * Déclaration de la classe Curve,
 * Permet la création et la manipulation de Curve
 * @class Curve
 * @param {Object} options  Données de la ligne
 * @constructor
 */
function Curve (options) {
  // Default options
  var defaultOptions = {
    id: null, // Id du point
    geomType: 'Curve',
    drawOnStart: true,
    // Callback_init
    callbackInit: function () {/* Do Nothing */
    }
  };

  // Merge options
  options = angular.extend({}, defaultOptions, options);
  var _self = this;
  this.eventActions = {
    'touchstart': function (e) {
      _self.startPainting();
      // _self.firstPoint = _self.setPoint(e);//no need
    },
    'mousedown': function (e) {
      _self.startPainting();
      //_self.firstPoint = _self.setPoint(e);
    },
    'mousemove': function () {
      if (_self.painting) {
        _self.draw(_self.previousPoint, _self.currentPoint);
      }
    },
    'touchmove': function () {
      if (_self.painting) {
        _self.draw(_self.previousPoint, _self.currentPoint);
      }
    },
    'mouseleave': function () {
      _self.stopPainting();
    },
    'touchcancel': function () {
      _self.stopPainting();
    },
    'mouseup': function () {
      _self.stopPainting();
    },
    'touchend': function () {
      _self.stopPainting();
    }
  };

  for (var key in options) {
    this[key] = options[key];
  }
  this.previousPoint = {
    x: 0,
    y: 0
  };

  this.currentPoint = {
    x: 0,
    y: 0
  };

  this.painting = false;

}

//Héritage Curve  hérie de common
Curve.prototype = Object.create(Common.prototype);
Curve.prototype.constructor = Curve;

/**
 * Prototype definition
 */

/*
 * ================================================================================================
 * ================================================================================================
 *
 * INITIALISATION
 *
 * ================================================================================================
 * ================================================================================================
 */

/**
 * Function init
 * Initialisation de la ligne
 * @return {Boolean} true si le point est correctement initialisé, faux sinon
 */
Curve.prototype.init = function () {
  return true;
};

/*
 * ================================================================================================
 * ================================================================================================
 *
 * GETTERS / SETTERS
 *
 * ================================================================================================
 * ================================================================================================
 */

/**
 * Function getGeomType
 * Retourne le type de géométrie
 * @class Point
 * @return
 */
Curve.prototype.getGeomType = function () {
  return this.geomType;
};

/*
 * ================================================================================================
 * ================================================================================================
 *
 * FONCTIONS
 *
 * ================================================================================================
 * ================================================================================================
 */

/**
 * Start Painting
 */
Curve.prototype.startPainting = function () {
  this.painting = true;
  var ctx = this.layers.drawing;
  ctx.beginPath();
  ctx.arc(this.currentPoint.x, this.currentPoint.y, this.toolOptions.lineWidth / 2, 0, 2 * Math.PI, false);
  ctx.fillStyle = this.toolOptions.strokeStyle;
  ctx.fill();
  ctx.closePath();

};

/**
 *Stop Painting
 */
Curve.prototype.stopPainting = function () {
  this.painting = false;
  if (!this.drawOnStart) return false;
};

Curve.prototype.onEvent = function (e) {
  var _self = this;
  _self.shiftKey = e.shiftKey;
  _self.previousPoint = this.currentPoint;
  _self.currentPoint = this.setPoint(e);

  if (_self.eventActions[e.type]) {//put in workers?
    return _self.eventActions[e.type].call(_self, e);//call pas apply car on passe argument e
  }

};

Curve.prototype.draw = function (previousPoint, currentPoint) {
  var ctx = this.layers.drawing;
  ctx.save();
  ctx.lineJoin = 'round';
  ctx.lineCap = 'round';
  ctx.beginPath();
  ctx.moveTo(previousPoint.x, previousPoint.y);
  ctx.lineTo(currentPoint.x, currentPoint.y);
  ctx.strokeStyle = this.toolOptions.strokeStyle;
  ctx.lineWidth = this.toolOptions.lineWidth;
  ctx.stroke();
  ctx.closePath();
  ctx.restore();
};
