/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* TrailEffectCurve Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe TrailEffectCurve, 
* Permet la création et la manipulation de Curve
* @class TrailEffectCurve
* @param {Object} options  Données de la ligne
* @constructor
*/
function TrailEffectCurve (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
TrailEffectCurve.prototype = Object.create(Common.prototype);
TrailEffectCurve.prototype.constructor = TrailEffectCurve;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
TrailEffectCurve.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
TrailEffectCurve.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
TrailEffectCurve.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
TrailEffectCurve.prototype.stopPainting = function () {
    this.painting = false;
};

TrailEffectCurve.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//use shiftkey do same as rectangle

TrailEffectCurve.prototype.draw = function () {
    var ctx = this.layers.drawing;
    var dist = this.distanceBetween(this.previousPoint, this.currentPoint);
    var angle = this.angleBetween(this.previousPoint, this.currentPoint);
    var x, y;
    ctx.fillStyle = this.toolOptions.fillStyle;
    ctx.strokeStyle = this.toolOptions.strokeStyle;

    for (var i = 0; i < dist; i += 5) {
        x = this.previousPoint.x + (Math.sin(angle) * i) - 25;
        y = this.previousPoint.y + (Math.cos(angle) * i) - 25;
        ctx.beginPath();
        ctx.arc(x + 10, y + 10, 20, false, Math.PI * 2, false);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
};
