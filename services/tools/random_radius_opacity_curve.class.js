/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* RandomRadiusOpacityCurve Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe RandomRadiusOpacityCurve, 
* Permet la création et la manipulation de Curve
* @class RandomRadiusOpacityCurve
* @param {Object} options  Données de la ligne
* @constructor
*/
function RandomRadiusOpacityCurve (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage RandomRadiusOpacityCurve hérie de cRandomRadiusOpacityCurve
RandomRadiusOpacityCurve.prototype = Object.create(Common.prototype);
RandomRadiusOpacityCurve.prototype.constructor = RandomRadiusOpacityCurve;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
RandomRadiusOpacityCurve.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
RandomRadiusOpacityCurve.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
RandomRadiusOpacityCurve.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
RandomRadiusOpacityCurve.prototype.stopPainting = function () {
    this.painting = false;
};

RandomRadiusOpacityCurve.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire deRandomRadiusOpacityCurve et gérer action danRandomRadiusOpacityCurves comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> fairRandomRadiusOpacityCurves
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//use shiftkey do same as rectangle

RandomRadiusOpacityCurve.prototype.draw = function () {
    var ctx = this.layers.drawing;
    ctx.save();
    ctx.lineJoin = ctx.lineCap = 'round';
    ctx.fillStyle = this.toolOptions.fillStyle;
    ctx.beginPath();
    ctx.globalAlpha = this.getRandomFloat(0.1, 1);
    ctx.arc(this.currentPoint.x, this.currentPoint.y, this.getRandomInt(10, 30), false, Math.PI * 2, false);
    ctx.fill();
    ctx.restore();
};
