/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* Circle Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe Circle, 
* Permet la création et la manipulation de Curve
* @class Circle
* @param {Object} options  Données de la ligne
* @constructor
*/
function Circle (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
Circle.prototype = Object.create(Common.prototype);
Circle.prototype.constructor = Circle;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
Circle.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
Circle.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
Circle.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
Circle.prototype.stopPainting = function () {
    this.painting = false;
};

Circle.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {//a mettre dans la classe common 
        case 'touchstart':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//use shiftkey do same as rectangle

Circle.prototype.draw = function () {
    var ctx = this.layers.drawing;
    this.clear(this.canvas.drawing);
    ctx.beginPath();
    var dx = this.currentPoint.x - this.firstPoint.x;
    var dy = this.currentPoint.y - this.firstPoint.y;
    // normalize
    var length = Math.sqrt(dx * dx + dy * dy);
    //perform a color gradient
    var grd = ctx.createRadialGradient(this.firstPoint.x, this.firstPoint.y, 0, this.currentPoint.x, this.currentPoint.y, length * 2);
    grd.addColorStop(0, this.toolOptions.fillStyle);
    grd.addColorStop(1, 'white');
    // Fill with gradient
    ctx.arc(this.firstPoint.x, this.firstPoint.y, length, 0, 2 * Math.PI, false);
    ctx.fillStyle = grd;
    ctx.fill();
    ctx.lineWidth = this.toolOptions.lineWidth;
    ctx.strokeStyle = this.toolOptions.strokeStyle;
    ctx.stroke();
    ctx.closePath();
};
