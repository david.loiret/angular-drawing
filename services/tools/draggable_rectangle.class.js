/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* DraggableRectangle Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe DraggableRectangle, 
* Permet la création et la manipulation de Curve
* @class DraggableRectangle
* @param {Object} options  Données de la ligne
* @constructor
*/
function DraggableRectangle (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'DraggableRectangle',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
DraggableRectangle.prototype = Object.create(Common.prototype);
DraggableRectangle.prototype.constructor = DraggableRectangle;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
DraggableRectangle.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
DraggableRectangle.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
DraggableRectangle.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
DraggableRectangle.prototype.stopPainting = function () {
    this.painting = false;
};

DraggableRectangle.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


DraggableRectangle.prototype.draw = function () {
    var ctx = this.layers.drawing;
    this.clear(this.canvas.drawing);
    ctx.beginPath();
    ctx.lineJoin = 'miter';
    ctx.lineCap = 'miter';
    var x = this.currentPoint.x;
    var y = this.currentPoint.y;
    var w = this.toolOptions.lineWidth;
    var h = w;
    //check shiftKey
    ctx.moveTo(this.previousPoint.x, this.previousPoint.y);
    ctx.rect(x, y, w, h);
    var grd = ctx.createLinearGradient(this.firstPoint.x, this.firstPoint.y, this.currentPoint.x, this.currentPoint.y);
    grd.addColorStop(0, 'black');
    grd.addColorStop(0.5, this.toolOptions.fillStyle);
    grd.addColorStop(1, 'white');
    //ctx.fillStyle = 'yellow';
    ctx.fillStyle = grd;
    ctx.fill();
    ctx.lineWidth = this.toolOptions.lineWidth;
    ctx.strokeStyle = this.toolOptions.strokeStyle;
    ctx.stroke();
    ctx.closePath();
};
