/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* Eraser Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe Eraser, 
* Permet la création et la manipulation de Curve
* @class Eraser
* @param {Object} options  Données de la ligne
* @constructor
*/
function Eraser (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

    this.params = options;

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
Eraser.prototype = Object.create(Common.prototype);
Eraser.prototype.constructor = Eraser;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
Eraser.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
Eraser.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
Eraser.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
Eraser.prototype.stopPainting = function () {
    this.painting = false;
};

Eraser.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};

Eraser.prototype.draw = function () {
    var ctx = this.layers.output;
    var oldcomposite = ctx.globalCompositeOperation;
    ctx.globalCompositeOperation = 'destination-out';
    ctx.save();
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.moveTo(this.previousPoint.x, this.previousPoint.y);
    ctx.lineTo(this.currentPoint.x, this.currentPoint.y);
    ctx.strokeStyle = this.toolOptions.strokeStyle;
    ctx.lineWidth = this.toolOptions.lineWidth;
    ctx.stroke();
    ctx.closePath();
    ctx.restore();
    ctx.globalCompositeOperation = oldcomposite;
};
