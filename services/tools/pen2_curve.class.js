/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* Pen2Curve Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe Pen2Curve, 
* Permet la création et la manipulation de Curve
* @class Pen2Curve
* @param {Object} options  Données de la ligne
* @constructor
*/
function Pen2Curve (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
Pen2Curve.prototype = Object.create(Common.prototype);
Pen2Curve.prototype.constructor = Pen2Curve;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
Pen2Curve.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
Pen2Curve.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
Pen2Curve.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
Pen2Curve.prototype.stopPainting = function () {
    this.painting = false;
};

Pen2Curve.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//use shiftkey do same as rectangle

Pen2Curve.prototype.draw = function () {
    var ctx = this.layers.drawing;
    ctx.save();
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = this.toolOptions.strokeStyle;
    ctx.beginPath();
    ctx.lineWidth = 1;//passer le line width

    ctx.moveTo(this.previousPoint.x - this.getRandomInt(0, 2), this.previousPoint.y - this.getRandomInt(0, 2));
    ctx.lineTo(this.currentPoint.x - this.getRandomInt(0, 2), this.currentPoint.y - this.getRandomInt(0, 2));
    ctx.stroke();

    ctx.moveTo(this.previousPoint.x, this.previousPoint.y);
    ctx.lineTo(this.currentPoint.x, this.currentPoint.y);
    ctx.stroke();

    ctx.moveTo(this.previousPoint.x + this.getRandomInt(0, 2), this.previousPoint.y + this.getRandomInt(0, 2));
    ctx.lineTo(this.currentPoint.x + this.getRandomInt(0, 2), this.currentPoint.y + this.getRandomInt(0, 2));
    ctx.stroke();
    ctx.closePath();
    ctx.restore();
};
