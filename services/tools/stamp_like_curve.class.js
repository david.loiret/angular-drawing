/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* StampLikeCurve Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe StampLikeCurve, 
* Permet la création et la manipulation de Curve
* @class StampLikeCurve
* @param {Object} options  Données de la ligne
* @constructor
*/
function StampLikeCurve (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
StampLikeCurve.prototype = Object.create(Common.prototype);
StampLikeCurve.prototype.constructor = StampLikeCurve;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
StampLikeCurve.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
StampLikeCurve.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
StampLikeCurve.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
StampLikeCurve.prototype.stopPainting = function () {
    this.painting = false;
};

StampLikeCurve.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//use shiftkey do same as rectangle

StampLikeCurve.prototype.draw = function () {
    var ctx = this.layers.drawing;
    ctx.save();
    var radius = 15;
    ctx.lineJoin = ctx.lineCap = 'round';
    ctx.strokeStyle = this.toolOptions.strokeStyle;
    ctx.fillStyle = this.toolOptions.fillStyle;
    ctx.beginPath();
    ctx.arc(this.currentPoint.x, this.currentPoint.y, radius, false, Math.PI * 2, false);
    ctx.fill();
    ctx.stroke();
    ctx.restore();
};
