/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* FlurCurve Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe FlurCurve, 
* Permet la création et la manipulation de Curve
* @class FlurCurve
* @param {Object} options  Données de la ligne
* @constructor
*/
function FlurCurve (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
FlurCurve.prototype = Object.create(Common.prototype);
FlurCurve.prototype.constructor = FlurCurve;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
FlurCurve.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
FlurCurve.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
FlurCurve.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
FlurCurve.prototype.stopPainting = function () {
    this.painting = false;
};

FlurCurve.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//use shiftkey do same as rectangle
FlurCurve.prototype.draw = function () {
    var ctx = this.layers.drawing;
    ctx.save();
    var self = this;
    var x;
    var y;
    var dist = this.distanceBetween(this.previousPoint, this.currentPoint);
    var angle = this.angleBetween(this.previousPoint, this.currentPoint);
  
    var img = new Image();
    img.src = 'app/modules/tb_drawing/assets/img/brush2.png';
    img.with = 10;

    img.onload = function () {
        for (var i = 0; i < dist; i++) {
            x = self.previousPoint.x + (Math.sin(angle) * i);
            y = self.previousPoint.y + (Math.cos(angle) * i);
            ctx.save();
            ctx.translate(x, y);
            ctx.scale(0.5, 0.5);
            ctx.rotate(Math.PI * 180 / self.getRandomInt(0, 180));
            ctx.drawImage(img, 0, 0);
            ctx.restore();
        }
    };
};
