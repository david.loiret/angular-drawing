/* ________________________________________________________________________________________________
* =================================================================================================
* DRAWING SYSTEM
* =================================================================================================
* date        : 28/02/2015
* auteur      : David LOIRET
*
* ShadowCurve Class
*
* ________________________________________________________________________________________________
*/

/**
* Déclaration de la classe ShadowCurve, 
* Permet la création et la manipulation de Curve
* @class ShadowCurve
* @param {Object} options  Données de la ligne
* @constructor
*/
function ShadowCurve (options) {
	// Default options
	var defaultOptions = {
		id:  null, // Id du point
		geomType: 'Curve',
		// Callback_init
		callbackInit: function() {/* Do Nothing */}    
	};

	// Merge options
	options = angular.extend({}, defaultOptions, options);

	for (var key in options) {
		this[key] = options[key]; 
	}
    this.previousPoint = {
        x: 0,
        y: 0
    };

    this.currentPoint = {
        x: 0,
        y: 0
    };

    this.painting = false;

}

//Héritage Curve  hérie de common 
ShadowCurve.prototype = Object.create(Common.prototype);
ShadowCurve.prototype.constructor = ShadowCurve;

/**
* Prototype definition
*/

/* 
* ================================================================================================
* ================================================================================================
* 
* INITIALISATION
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function init
* Initialisation de la ligne
* @return {Boolean} true si le point est correctement initialisé, faux sinon
*/
ShadowCurve.prototype.init = function() {
	return true;
};

/* 
* ================================================================================================
* ================================================================================================
* 
* GETTERS / SETTERS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Function getGeomType
* Retourne le type de géométrie
* @class Point
* @return  
*/
ShadowCurve.prototype.getGeomType = function() {
	return this.geomType;
}; 

/* 
* ================================================================================================
* ================================================================================================
* 
* FONCTIONS
* 
* ================================================================================================
* ================================================================================================
*/

/**
* Start Painting
*/
ShadowCurve.prototype.startPainting = function() {
    this.painting = true;
};

/**
*Stop Painting
*/
ShadowCurve.prototype.stopPainting = function () {
    this.painting = false;
};

ShadowCurve.prototype.onEvent = function (e) {
	
	this.shiftKey = e.shiftKey;
	this.previousPoint = this.currentPoint;
	this.currentPoint = this.setPoint(e);

    switch (e.type) {
        //faire des new  et gérer action dans les s comme webpaint
        case 'touchstart':
            //marche pas ds le cas de la ligne simple ou on veut un point au touchstart -> faire des s
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousedown':
            this.startPainting();
            this.firstPoint = this.setPoint(e);
            break;
        case 'mousemove':
            if (this.painting) {
                this.draw();
            }
            break;
        case 'touchmove':
            if (this.painting) {
                this.draw();
            }
            break;

        case 'mouseleave':
            this.stopPainting();
            break;

        case 'touchcancel':
            this.stopPainting();
            break;

        case 'mouseup':
            this.stopPainting();
            break;

        case 'touchend':
            this.stopPainting();
            break;
    }

};


//use shiftkey do same as rectangle

ShadowCurve.prototype.draw = function () {
    var ctx = this.layers.drawing;
    ctx.save();//save and restore
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.shadowBlur = this.toolOptions.shadowBlur;
    ctx.shadowColor = this.toolOptions.shadowColor;
    ctx.beginPath();
    ctx.moveTo(this.previousPoint.x, this.previousPoint.y);
    ctx.lineTo(this.currentPoint.x, this.currentPoint.y);
    ctx.strokeStyle = this.toolOptions.strokeStyle;
    ctx.lineWidth = this.toolOptions.lineWidth;
    ctx.stroke();
    ctx.closePath();
    ctx.restore();//remove properties as shadow
    //http://stackoverflow.com/questions/4649883/html-canvas-shadow-being-applied-to-everything
};
