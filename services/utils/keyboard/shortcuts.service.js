(function () {

    'use strict';

    angular
        .module('tbDrawing')
        .factory('shortcuts', shortcuts);

    function shortcuts() {


        var shortcuts = function () {
            this.shortcuts_ = {};
            this.init();
        };

        shortcuts.proxy = function (fn, context) {
            var args, tmp;

            if (typeof context === "string") {
                tmp = fn[context];
                context = fn;
                fn = tmp;
            }

            // Quick check to determine if target is callable, in the spec
            // this throws a TypeError, but we will just return undefined.
            if (!(typeof fn === 'function')) {
                return undefined;
            }

            // Simulated bind
            args = slice.call(arguments, 2);

            return fn.apply(context || this, args.concat(slice.call(arguments)));


            // Set the guid of unique handler to the same of original handler, so it can be removed
            //proxy.guid = fn.guid = fn.guid || jQuery.guid++;

            //return proxy;

        };

        shortcuts.prototype.keyCodeTranslator = function (keycode) {
            var specialKeys = {
                191: "?",
                8: "back",
                27: "esc",
                38: "up",
                40: "down",
                46: "del",
                189: "-",
                187: "+",
                188: "<",
                190: ">"
            };

            if (keycode >= 48 && keycode <= 57) {
                // key is 0-9
                return (keycode - 48) + "";
            } else if (keycode >= 65 && keycode <= 90) {
                // key is a-z, use base 36 to get the string representation
                return (keycode - 65 + 10).toString(36);
            } else {
                return specialKeys[keycode];
            }

        };

        shortcuts.prototype.isMac() = function {
            var ua = navigator.userAgent;
            return /Mac/.test(ua);
        };
        /**
         * @public
         */
        shortcuts.prototype.init = function () {
            var _self = this;
            document.addEventListener('keydown', function(e) {
                var key = e.keyCode || e.which;
                _self.proxy(this.onKeyUp_, this)
            });
            //$(document.body).keydown(_self.proxy(this.onKeyUp_, this));
        };

        /**
         * Add a keyboard shortcut
         * @param {String}   rawKey   (case insensitive) key can be a meta (optional) + [a-z0-9] or
         *                            a special key (check list of supported keys in KeycodeTranslator)
         *                            eg. 'ctrl+A', 'del'
         * @param {Function} callback should return true to let the original event perform its default action
         */
        shortcuts.prototype.addShortcut = function (rawKey, callback) {
            var parsedKey = this.parseKey_(rawKey.toLowerCase());

            var key = parsedKey.key,
                meta = parsedKey.meta;

            this.shortcuts_[key] = this.shortcuts_[key] || {};

            if (this.shortcuts_[key][meta]) {
                var keyStr = (meta !== 'normal' ? meta + ' + ' : '') + key;
                console.error('[ShortcutService] >>> Shortcut [' + keyStr + '] already registered');
            } else {
                this.shortcuts_[key][meta] = callback;
            }
        };

        shortcuts.prototype.addShortcuts = function (keys, callback) {
            keys.forEach(function (key) {
                this.addShortcut(key, callback);
            }.bind(this));
        };

        shortcuts.prototype.removeShortcut = function (rawKey) {
            var parsedKey = this.parseKey_(rawKey.toLowerCase());

            var key = parsedKey.key,
                meta = parsedKey.meta;

            this.shortcuts_[key] = this.shortcuts_[key] || {};

            this.shortcuts_[key][meta] = null;
        };

        shortcuts.prototype.parseKey_ = function (key) {
            var meta = this.getMetaKey_({
                alt: key.indexOf('alt+') != -1,
                shift: key.indexOf('shift+') != -1,
                ctrl: key.indexOf('ctrl+') != -1
            });

            var parts = key.split(/\+(?!$)/);
            key = parts[parts.length - 1];
            return {meta: meta, key: key};
        };

        shortcuts.prototype.getMetaKey_ = function (meta) {
            var keyBuffer = [];
            ['alt', 'ctrl', 'shift'].forEach(function (metaKey) {
                if (meta[metaKey]) {
                    keyBuffer.push(metaKey);
                }
            });

            if (keyBuffer.length > 0) {
                return keyBuffer.join('+');
            } else {
                return 'normal';
            }
        };
        /**
         * @private
         */
        shortcuts.prototype.onKeyUp_ = function (evt) {
            if (!this.isInInput_(evt)) {
                // jquery names FTW ...
                var keycode = evt.which;
                var targetTagName = evt.target.nodeName.toUpperCase();
                var charkey = this.keycodeTranslator(keycode);

                var keyShortcuts = this.shortcuts_[charkey];
                if (keyShortcuts) {
                    var meta = this.getMetaKey_({
                        alt: this.isAltKeyPressed_(evt),
                        shift: this.isShiftKeyPressed_(evt),
                        ctrl: this.isCtrlKeyPressed_(evt)
                    });
                    var cb = keyShortcuts[meta];

                    if (cb) {
                        var bubble = cb(charkey);
                        if (bubble !== true) {
                            evt.preventDefault();
                        }
                    }
                }
            }
        };

        shortcuts.prototype.isInInput_ = function (evt) {
            var targetTagName = evt.target.nodeName.toUpperCase();
            return targetTagName === 'INPUT' || targetTagName === 'TEXTAREA';
        };

        shortcuts.prototype.isCtrlKeyPressed_ = function (evt) {
            return this.isMac() ? evt.metaKey : evt.ctrlKey;
        };

        shortcuts.prototype.isShiftKeyPressed_ = function (evt) {
            return evt.shiftKey;
        };

        shortcuts.prototype.isAltKeyPressed_ = function (evt) {
            return evt.altKey;
        };

        return shortcuts;

    }


})();