(function () {

  'use strict';

  angular
    .module('tbDrawing')
    .directive('tbDrawMe', tbDrawMe);

  /**
   * @ngInject
   */
  function tbDrawMe ($log, TbDrawing, $timeout, $window) {

    return {
      restrict: 'AE',
      template: '<div></div>',
      replace: true,
      transclude: true,
      link: link,
      scope: {
        background: '=',
        width: '=',
        height: '=',
        drawingControls: '=',
        filterControls: '='
      }
    };

    function link (scope, element, attrs) {
      //set size
      var backgroundCanvas = document.createElement('canvas');
      backgroundCanvas.width = scope.width;
      backgroundCanvas.height = scope.height;
      backgroundCanvas.setAttribute('id', 'background-canvas');
      element.append(backgroundCanvas);

      var outputCanvas = document.createElement('canvas');
      outputCanvas.width = scope.width;
      outputCanvas.height = scope.height;
      outputCanvas.setAttribute('id', 'output-canvas');
      element.append(outputCanvas);

      var drawingCanvas = document.createElement('canvas');
      drawingCanvas.width = scope.width;
      drawingCanvas.height = scope.height;
      drawingCanvas.setAttribute('id', 'drawing-canvas');
      element.append(drawingCanvas);

      var canvas = {
        output: outputCanvas,
        background: backgroundCanvas,
        drawing: drawingCanvas
      };
      //passer valeur depart

      var oDrawing = new TbDrawing({background: scope.background, defaultTool: scope.$parent.tool}, canvas);
      element.bind('click mousedown mouseup mousemove mouseleave mouseout touchstart touchmove touchend touchcancel', onEvent);

      scope.$on('$destroy', function () {
        element.unbind();
      });

      function onEvent (e) {
        oDrawing.onEvent(e);
      }

      /*angular.element($window).bind('resize', function () {
       oDrawing.resize($window.innerWidth, $window.innerHeight);
       });*/


      //faire fonction redraw on resize
      //live filter?

      scope.$parent.$watch('strokeStyle', function (strokeStyle) {//@TODO suppress watcher a mettre dans controller?
        if (strokeStyle !== undefined) {
          oDrawing.setStrokeStyle(strokeStyle);
        }
      });

      scope.$parent.$watch('fillStyle', function (fillStyle) {//@TODO suppress watcher a mettre dans controller?
        if (fillStyle !== undefined) {
          oDrawing.setFillStyle(fillStyle);
        }
      });

      scope.$parent.$watch('layer', function (layer) {//@TODO suppress watcher a mettre dans controller?
        if (layer !== undefined) {
          oDrawing.setSelectedLayer(layer.id);
        }

      });

      //a mettre dans class filter

      scope.drawingControls.setTool = function (tool) {//@TODO: a mettre dans controller
        scope.$parent.tool = tool;
        oDrawing.setTool(tool);
      };

      scope.drawingControls.setLineWidth = function (lineWidth) {//@TODO: a mettre dans controller
        oDrawing.setLineWidth(lineWidth);
      };

      scope.drawingControls.undo = function () {
        oDrawing.undo();
      };

      scope.drawingControls.addImage = function (image, layer, size) {
        oDrawing.addImage(image, layer, size);
      };

      scope.drawingControls.redo = function () {
        oDrawing.redo();
      };

      if (scope.filterControls) {

        scope.filterControls.sharpen = function () {
          oDrawing.filterImage('convolute',
            [0, -1, 0,
              -1, 5, -1,
              0, -1, 0]);
        };

        scope.filterControls.custom = function () {
          oDrawing.filterImage('convolute',
            [1, 1, 1,
              1, 0.7, -1,
              -1, -1, -1]);
        };

        scope.filterControls.edgeDetect = function () {//filtre laplacien
          oDrawing.filterImage('convolute',
            [0, 1, 0,
              1, -4, 1,
              0, 1, 0]);
        };

        scope.filterControls.edgeDetect2 = function () {//filtre laplacien
          oDrawing.filterImage('convolute',
            [1, 1, 1,
              1, -8, 1,
              1, 1, 1]);
        };

        scope.filterControls.edgeEnhance = function () {
          oDrawing.filterImage('convolute',
            [0, 0, 0,
              -1, 1, 0,
              0, 0, 0]);
        };

        scope.filterControls.gaussianBlur = function () {
          oDrawing.filterImage('convolute',
            [.1, .1, .1,
              .1, .2, .1,
              .1, .1, .1]);
        };


        scope.filterControls.emboss = function () {
          oDrawing.filterImage('convolute',
            [-2, -1, 0,
              -1, 1, 1,
              0, 1, 2]);
        };

        scope.filterControls.sobel = function () {
          oDrawing.testsobel();
        };

        scope.filterControls.blur = function (nb) {
          nb = 1 / nb;
          oDrawing.filterImage('convolute',
            [nb, nb, nb,
              nb, nb, nb,
              nb, nb, nb]);
        };

        scope.filterControls.motionBlur = function () {
          oDrawing.filterImage('convolute',
            [1, 0, 0,
              0, 1, 0,
              0, 0, 1]);
        };

        scope.filterControls.filter = function (filter, args) {
          oDrawing.filterImage(filter, args);
        };

        scope.filterControls.setColor = function (color) {
          oDrawing.setColor(color);
        };

        scope.filterControls.eraser = function () {
          scope.$parent.tool = 'Eraser';
          oDrawing.setTool('Eraser');
        };

      }

      scope.drawingControls.clear = function () {
        oDrawing.rmAll();
      };

      scope.drawingControls.download = function (open, options) {//a changer
        return oDrawing.download(options, open);
      };
    }


  }

}());
